/**
 * 
 */
package it.unibo.oop.lab.enum2;

import java.util.*;
import java.util.Map.Entry;

import it.unibo.oop.lab.enum2.Sport;
import it.unibo.oop.lab.socialnetwork.SocialNetworkUserImpl;
import it.unibo.oop.lab.socialnetwork.User;

/**
 * 
 * Represents a social network user along with the sports he/she likes to do or
 * to follow.
 * 
 * It is the same class as previous exercises but with new methods to be
 * implemented
 * 
 * 
 * 1) Reuse the same code you have already written in the previous
 * SportSocialNetworkUserImpl as far as methods implemented before are concerned
 * 
 * 2) complete the implementation of the new methods as explained below
 * 
 * @param <U>
 *            specific {@link User} type
 */
public class Sport2SocialNetworkUserImpl<U extends User> extends SocialNetworkUserImpl<U> {

    private final Set<Sport> followed= new TreeSet<Sport>();
	private final Map<String, Set<U>> friends;


    /**
     * Builds a new {@link Sport2SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public Sport2SocialNetworkUserImpl(final String name, final String surname, final String user) {
        this(name, surname, user, -1);
    }

    /**
     * Builds a new {@link Sport2SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public Sport2SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
        this.friends = new HashMap<>();
    }

    /**
     * add an User to a friend circle. If the circle does not exist, it create a new one
     * @param circle
     * 		The circle of friends
     * @param user
     * 		The user to add to a circle 
     */
    public boolean addFollowedUser(final String circle, final U user) {
        Set<U> circleFriends = this.friends.get(circle);
        if (circleFriends == null) {
            circleFriends = new HashSet<>();
            this.friends.put(circle, circleFriends);
        }
        return circleFriends.add(user);
    }

    /**
     *
     * If no group with groupName exists yet, this implementation must
     * return an empty Collection.
     * 
     * {@inheritDoc}
     */
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
        final Collection<U> usersInCircle = this.friends.get(groupName);
        if (usersInCircle != null) {
            return new ArrayList<>(usersInCircle);
        }
        /*
         * Return a very fast, lightweight, unmodifiable, pre-cached empty list.
         */
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    public List<U> getFollowedUsers() {
        /*
         * Pre-populate a Set in order to prevent duplicates
         */
        final Set<U> followedUsers = new HashSet<>();
        for (final Entry<String, Set<U>> group : friends.entrySet()) {
            followedUsers.addAll(group.getValue());
        }
        return new ArrayList<>(followedUsers);
    }

    /**
     * Returns true if a user likes/does a given sport.
     * 
     * @param s
     *            sport to use as an input
     * 
     * @return true if the user likes sport s
     */
    public boolean hasSport(final Sport s) {
        return followed.contains(s);
    }

    /**
     * Add a new sport followed by this user: if the user already likes or does
     * the sport, nothing happens.
     * 
     * @param sport
     *            a sport followed/done by the user
     */
    public void addSport(final Sport sport) {
    		followed.add(sport);
    }

    /**
     * Returns true if a user likes/does a given sport.
     * 
     * @param s
     *            sport to check
     * 
     * @return true if user likes sport s
     */
    public boolean likesSport(final Sport s) {
        return followed.contains(s);
    }

    /*
     * [METHODS] NEW METHODS TO IMPLEMENT FROM SCRATCH
     */

    /**
     * Returns the set of individual sports followed/practiced by this user: a
     * sport is individual if the number of team member is = 1.
     * 
     * @return the set of individual sport this user practices/follows
     */
    public Set<Sport> getIndividualSports(){
    	Set<Sport> ret= new LinkedHashSet<>();
    	for(Sport s: followed) {
    		if(s.isIndividualSport()) {
    			ret.add(s);
    		}
    	}
    	return ret;
    }
    
     /** Returns the set of sports which are practiced in a given place.
     * 
     * @param p the place in which the sport is practiced in order to be
     * included in the resulting set
     * 
     * @return the set of sport practiced in a given place
     */
     
     public Set<Sport> getSportPracticedInPlace(Place p){
    	 Set<Sport> ret= new LinkedHashSet<>();
     	for(Sport s: followed) {
     		if(s.getPlace()==p) {
     			ret.add(s);
     		}
     	}
     	return ret;
     }
     
}
