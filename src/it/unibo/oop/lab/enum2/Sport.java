/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

	BASKET(Place.INDOOR, 6, "basketball"),
	SOCCER(Place.OUTDOOR, 6, "soccer"),
	TENNIS(Place.OUTDOOR, 1, "tennis"),
	BIKE(Place.OUTDOOR, 1, "bike"),
	F1(Place.OUTDOOR, 1, "Formula 1"),
    MOTOGP(Place.OUTDOOR, 1, "Moto gp");
    
    final int teamNumber;
    final Place place;
    final String name;
    
    /**
     * 
     * @param place
     * 		a param indicating if the sport is praticated indoor or outdoor {@link Place}
     * @param noTeamMembers
     * 		number of team this sport require
     * @param actualName
     * 		name of this sport
     */
    Sport(final Place place, final int noTeamMembers, final String actualName){
    	this.place=place;
    	this.teamNumber=noTeamMembers;
    	this.name=actualName;
    }
    
    public boolean isIndividualSport() {
    	return teamNumber==1;
    }
    
    
    /**
     * 
     * @return
     * 		return a boolean indicating if this sport is indoor
     */
    public boolean isIndoorSport() {
    	return this.place == Place.INDOOR;
    }
    
    /**
     * @return
     * 		return the place of this sport {@link Place}
     *
     */
    public Place getPlace() {
    	return place;
    }
    
    
    /**
     * @return 
     * 		return a line description for this sport
     */
    public String toString() {
    	return name+": Place:"+place.toString()+", TeamMember: "+teamNumber;
    }


}
