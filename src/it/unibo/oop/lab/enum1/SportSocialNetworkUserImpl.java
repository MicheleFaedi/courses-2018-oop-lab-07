/**
 * 
 */
package it.unibo.oop.lab.enum1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import it.unibo.oop.lab.socialnetwork.SocialNetworkUserImpl;
import it.unibo.oop.lab.socialnetwork.User;

/**
 * 
 * Represents a social network user along with the sports he/she likes to do or
 * to follow.
 * 
 * 1) Define the same behavior as done on the previous exercise: - you can reuse
 * the same class, but... - ..now make explicit reference to an enumeration
 * Sport rather than a nested static class
 * 
 * 
 * - NOTE: now we going to define Sport as an enumeration (in its own file
 * Sport.java)
 * 
 * 
 *
 * @param <U>
 *            specific user type
 */
public class SportSocialNetworkUserImpl<U extends User> extends SocialNetworkUserImpl<U> {

	
	private final List<Sport> sports= new ArrayList<>();
	private final Map<String, Set<U>> friends;

    /**
     * Builds a new {@link SportSocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SportSocialNetworkUserImpl(final String name, final String surname, final String user) {
        this(name, surname, user, -1);
    }

    /**
     * Builds a new {@link SportSocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SportSocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
        this.friends = new HashMap<>(); // inference of type variables
    }

    
    /**
     * add an User to a friend circle. If the circle does not exist, it create a new one
     * @param circle
     * 		The circle of friends
     * @param user
     * 		The user to add to a circle 
     */
    public boolean addFollowedUser(final String circle, final U user) {
        Set<U> circleFriends = this.friends.get(circle);
        if (circleFriends == null) {
            circleFriends = new HashSet<>();
            this.friends.put(circle, circleFriends);
        }
        return circleFriends.add(user);
    }

    /**
     *
     * If no group with groupName exists yet, this implementation must
     * return an empty Collection.
     * 
     * {@inheritDoc}
     */
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
        final Collection<U> usersInCircle = this.friends.get(groupName);
        if (usersInCircle != null) {
            return new ArrayList<>(usersInCircle);
        }
        /*
         * Return a very fast, lightweight, unmodifiable, pre-cached empty list.
         */
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    public List<U> getFollowedUsers() {
        /*
         * Pre-populate a Set in order to prevent duplicates
         */
        final Set<U> followedUsers = new HashSet<>();
        for (final Entry<String, Set<U>> group : friends.entrySet()) {
            followedUsers.addAll(group.getValue());
        }
        return new ArrayList<>(followedUsers);
    }
    /**
     * Add a new sport followed by this user: if the user already likes or does
     * the sport, nothing happens.
     * 
     * @param sport
     *            a sport followed/done by the user
     */
    public void addSport(final Sport sport) {
    	sports.add(sport);
    }

    /**
     * Returns true if a user likes/does a given sport.
     * 
     * @param s
     *            sport to use as an input
     * 
     * @return true if the user likes sport s
     */
    public boolean hasSport(final Sport s) {
        return sports.contains(s);
    }
}
