package it.unibo.oop.lab.nesting2;

import java.util.List;
import java.util.Objects;

public class OneListAcceptable<T> implements Acceptable<T> {

	int index=0;
	List<T> list;
	OneListAcceptable(List<T> list){
		this.list=Objects.requireNonNull(list, "list must not be null");
	}
	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>(){
			int n=0;
			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(!list.contains(newElement))
					throw new ElementNotAcceptedException(newElement);
				n++;
			}
			@Override
			public void end() throws EndNotAcceptedException {
				if(n!=list.size())
					throw new EndNotAcceptedException();
			}
			
		};
	}

}
